\name{casinoTrips}
\alias{casinoTrips}
\docType{data}
\title{
Casino Trip Data
}
\description{
Individual level gambling and comp activity of patrons at a casino.  Patron level information can be found in the \code{casinoPatrons} data frame.
}
\usage{data(casinoTrips)}
\format{
  A data frame with 26293 observations on the following 20 variables.
  \describe{
    \item{\code{ID}}{Primary key to identify casino patrons}
    \item{\code{StartDate}}{Starting date of the casino trip}
    \item{\code{EndDate}}{Ending date of the casino trip}
    \item{\code{Event}}{Dummy code for whether the patron is attending an invitation-only special event (e.g., a slot tournament)}
    \item{\code{Promo}}{Dummy code for whether patron is redeeming a non-event promotion}
    \item{\code{PromoCredits}}{Value of promotional credits redeemed.  Promotional credits can be generated from marketing offers sent prior to arrival or casino hosts during the trip.}
    \item{\code{Res}}{Dummy code for whether the patron is staying at the casino's hotel}
    \item{\code{PlayDays}}{Days with gambling activity}
    \item{\code{DaysTable}}{Days with table game gambling (e.g., roulette, blackjack)}
    \item{\code{DaysSlot}}{Days with slot gambling (including video poker)}
    \item{\code{TableTime}}{Minutes playing table games}
    \item{\code{SlotTime}}{Minutes playing slot games}
    \item{\code{Theo}}{Total theoretical win}
    \item{\code{SlotTheo}}{Slot theoretical win}
    \item{\code{CasinoWin}}{Actual casino win, net of promotional credit payouts}
    \item{\code{SlotCasWin}}{Actual slot casino win, net of promotional credit payouts}
    \item{\code{CompNights}}{Complimentary hotel room nights}
    \item{\code{RoomComp}}{Value of room complimentaries}
    \item{\code{OtherComp}}{Value of non-room complimentaries (e.g., restaurants, show tickets, etc.)}
    \item{\code{authcomp}}{Authorized complimentary amount based on total playing time, types of games played, and the casino's target reinvestment rate}
  }
}
\source{
Wayne Taylor
}
\examples{
data(casinoTrips)

#Distribution of trip lengths (excluding trips longer than 10 days)
hist(as.numeric(casinoTrips$EndDate-casinoTrips$StartDate+1),breaks=50,xlim=range(1,10),xlab="Trip Days",main="Distribution of Trip Lengths")

#Plot of actual vs theoretical win for low-end players
plot(CasinoWin~Theo,data=casinoTrips[casinoTrips$Theo<500,])
abline(lm(CasinoWin~Theo,data=casinoTrips[casinoTrips$Theo<500,]),col="red")
}
\keyword{datasets}
