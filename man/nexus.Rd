\name{nexus}
\alias{nexus}
\docType{data}
\title{
NEXUS dataset
}
\description{
Each of the following 8 variables have been measured for 10,000 YouTube user accounts.
}
\usage{data(bank)}
\format{
  A data frame with 10,000 observations on the following 8 variables.
  \describe{
    \item{\code{watched.promo}}{1 if promo was watched and 0 otherwise}
    \item{\code{bought.nexus}}{1 if the branch is in Manhattan and 0 otherwise}
    \item{\code{num.videos.watched.in.last.6.months}}{Number of total YouTube videos watched in the last 6 months}
    \item{\code{num.videos.watched.in.electronics}}{Number of electronics YouTube videos watched during the life of the user account}
    \item{\code{browser.type}}{One of Chrome, Firefox, MsExplorer, or Safari}
    \item{\code{device.type}}{One of Desktop or Mobile}
    \item{\code{age}}{User-reported age}
    \item{\code{gender}}{One of M or F}
  }
}
\details{
YouTube user behavior related to whether the user watched a YouTube video to promote the Nexus tablet and whether the tablet was purchased.
}
\examples{
data(nexus)
}
\keyword{datasets}
