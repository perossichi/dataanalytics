\name{hmsExpend}
\alias{hmsExpend}
\docType{data}
\title{
Homescan Aggregate Expenditure Data
}
\description{
Data on Expenditures of Homescan Panelists by Retailer and Product Group
}
\usage{data(hmsExpend)}
\format{
  A data frame with 814076 observations on the following 8 variables.
  \describe{
    \item{\code{department_code}}{a factor with levels for each of main 9 depts of a stores}
    \item{\code{product_group_code}}{a factor with levels for each of main "groups" of products}
    \item{\code{product_module_code}}{a factor with levels for each product module}
    \item{\code{retailer_code}}{a factor with levels \code{All Retailers} \code{SAFEWAY} \code{KROGER} \code{MEIJER} \code{PUBLIX} \code{TARGET REG} \code{WAL MART REG} \code{SAMS} \code{COSTCO}}
    \item{\code{expend}}{a numeric vector}
    \item{\code{expend_PL}}{a numeric vector}
    \item{\code{year}}{a numeric vector}
    \item{\code{month}}{a numeric vector}
  }
}
\details{
These are data from the Homescan panel maintained by Nielsen.  There are more than 30,000 panelists located across the United States. This is data aggregated up to the product module,month,retailer level. That is, the expenditure variables represent the sum of expenditures by Homescan panelists for a given month in a given product module and at a given retailer.  

Products are classified into one modules.  Each group is made of a set of modules. Each department is made up of a set of groups. 
}
\source{
This data is from the Kilts Center for Marketing, Booth School of Business, Univeristy of Chicago. The data can only be used for classroom purposes for the Anderson/UCLA class 264B.  \emph{No other uses are permitted.} 
}

\examples{
data(hmsExpend)

}
\keyword{datasets}
