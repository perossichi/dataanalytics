\name{sp500}
\alias{sp500}
\docType{data}

\title{Time series of monthly returns for the S&P500 and U.S. common stocks}

\description{
Monthly stock return data for the S&P 500 and 1,670 U.S. common stocks from January 2000 through June 2016. Returns include dividends. Stocks included are those that have a complete time series available from CRSP over the period selected.
}

\usage{data(SP500)}

\format{
A list of 3 objects:
  \describe{
    \item{\code{sp}}{an xts time series of S&P 500 Index returns}
    \item{\code{stocks}}{an xts time series of 1,670 U.S. common stock returns}
    \item{\code{comp_names}}{a data frame listing the PERMNO, ticker, and company name of included stocks}
  }
}

\details{
Data are stored as xts objects, which can be used most effectively with the 'xts' package. See example below.
}

\source{
Compiled from data made available through CRSP U.S. Stock Databases and CRSP Historical Indexes, Center for Research in Security Prices (CRSP), The University of Chicago Booth School of Business.
}

\references{
CRSP documentation available online at \url{http://www.crsp.com/documentation}.
}

\examples{
# install.packages("xts")
library(xts)

data(SP500)
dat = merge.xts(SP500$sp, SP500$stocks)
cor(dat[,1:5])
plot(dat$vwretd, main = "SP500 Returns")
}

\keyword{datasets}
