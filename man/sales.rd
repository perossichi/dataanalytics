\name{sales}
\alias{sales}
\docType{data}
\title{
Sales by Account by Product
}
\description{
Sales of Two Telecom Products by Customer Account
}
\usage{data(sales)}
\format{
  A data frame with 112363 observations on the following 4 variables.
  \describe{
    \item{\code{Customer_Id}}{a numeric vector}
    \item{\code{quarter}}{a numeric vector:  2010,qtr 1 is 1}
    \item{\code{product}}{a factor with levels \code{Prd1} \code{Prd2}}
    \item{\code{sales}}{a numeric vector - $ sales includes returns}
  }
}
\details{
Customer Account Data -- each record is a transaction -- there can be multiple transactions per quarter
}
\source{
obtained from a FEMBA student
}
\examples{
data(sales)
hist(sales$sales)
}
\keyword{datasets}
