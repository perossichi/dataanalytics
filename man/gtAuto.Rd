\name{gtAuto}
\alias{gtAuto}
\docType{data}
\title{
Google Trends Motor Vehicle and Parts Sales
}
\description{
Motor vehcile and parts sales with web search interest on related terms from January 2004 through July 2011.
}
\usage{data(gtAuto)}
\format{
  A data frame with 91 observations on the following 4 variables.
  \describe{
    \item{\code{Period}}{Date}
    \item{\code{sales}}{Reported sales in millions of Dollars}
    \item{\code{suvs}}{Percent change in the "Trucks & SUVs" search category}
    \item{\code{insurance}}{Percent change in the "Automotive Insurance" search category}
  }
}
\details{
The search category percent changes are based on the first Sunday-Saturday in the period, not the full month.  Google classifies search queries into about 30 categories at the top level and about 250
categories at the second level using a natural language classification engine.
}
\source{
"Motor Vehicles and Parts Dealers" series from the U.S. Census Bureau "Advance Monthly Sales for Retail and Food Services" report \emph{http://www.census.gov/retail/marts/www/timeseries.html}
}
\references{
Choi, H., & Varian, H. (2012). Predicting the Present with Google Trends. \emph{Economic Record}, 88(s1), 2-9.
}
\examples{
data(gtAuto)

#Time series model
library(DataAnalytics)
y<-log(gtAuto$sales)
autos<-data.frame(cbind(y,back(y),back(y,12),gtAuto$suvs,gtAuto$insurance))
colnames(autos)<-c("y","lagy1","lagy12","suvs","insurance")
summary(lm(y~lagy1+lagy12,data=autos))
}
\keyword{datasets}
